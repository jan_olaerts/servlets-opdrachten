package be.janolaerts.servlets.Opdracht2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

//@WebServlet(name = "be.janolaerts.servlets.Opdracht2.GreetingServlet",
//            value = "/Greeting",
//            initParams = @WebInitParam(name="text", value="Hello World"))
public class GreetingServlet extends HttpServlet {

    private String text;
    private String size;

    public GreetingServlet() {

    }

    public void init() throws ServletException {
        new GreetingServlet();
        log("Servlet object made");
        text = getInitParameter("text");
        size = getInitParameter("size");
        if(text == null) throw new ServletException("Parameter text not found");
    }

    public void destroy() {
        log("Servlet object garbage collected");
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Hello World Servlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<p style=font-size:" + size +"px;>" + text + "</p>");
            out.println("</body>");
            out.println("</html>");
        }
    }
}