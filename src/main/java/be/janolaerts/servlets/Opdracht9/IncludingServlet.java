package be.janolaerts.servlets.Opdracht9;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/Including")
public class IncludingServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        request.getSession();

        try(PrintWriter out = response.getWriter()) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Including Servlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<div>Including including.html</div>");
//            RequestDispatcher disp = getServletContext().getNamedDispatcher("VisitorIncludeServlet");
            RequestDispatcher disp = request.getRequestDispatcher("/HTML/including.html");
            disp.include(request, response);
            out.println("</body>");
            out.println("</html>");
        }
    }
}