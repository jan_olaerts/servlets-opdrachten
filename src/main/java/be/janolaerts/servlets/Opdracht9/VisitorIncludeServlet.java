package be.janolaerts.servlets.Opdracht9;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static be.janolaerts.servlets.Opdracht6.VisitorSessionListener.ACTIVE;
import static be.janolaerts.servlets.Opdracht6.VisitorSessionListener.TOTAL;

@WebServlet(name = "VisitorIncludeServlet", value = "/VisitorInclude")
public class VisitorIncludeServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int total = (Integer) getServletContext().getAttribute(TOTAL);
        int active = (Integer) getServletContext().getAttribute(ACTIVE);

        @SuppressWarnings("resource")
        PrintWriter out = response.getWriter();
        out.println("Total visitors: " + total + "<br/>");
        out.println("Current visitors: " + active + "<br/>");
    }
}