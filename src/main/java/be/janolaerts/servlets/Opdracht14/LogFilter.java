package be.janolaerts.servlets.Opdracht14;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

//@WebFilter(urlPatterns = "/*")
public class LogFilter extends HttpFilter {

    @Override
    public void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {

        LocalDateTime now = LocalDateTime.now();
        String message = String.format("%td/%<tm/%<tY %<tH:%<tM:%<tS %s %s\n", now, req.getRequestURL(), req.getRemoteHost());
        req.getServletContext().log(message);
        chain.doFilter(req, res);
    }
}