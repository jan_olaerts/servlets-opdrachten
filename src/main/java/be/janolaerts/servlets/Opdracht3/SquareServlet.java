package be.janolaerts.servlets.Opdracht3;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/Square")
public class SquareServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String errorMessage = "";
        double number = 0d;
        double square = 0d;
        String output = null;

        request.setCharacterEncoding("UTF-8");

        try {
            number = Double.parseDouble(request.getParameter("number"));
            square = Math.pow(number, 2);
            output = number + "squared is" + square;
        } catch (NumberFormatException nfe) {
            errorMessage = "The input is not a number!";
        }

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html");
            out.println("<html><head><title>");
            out.println("Square Servlet");
            out.println("</title></head><body>");

            out.println("<p> ");
            if(output != null) out.println(output);
            else out.println(errorMessage);
            out.println(" </p>");

            out.println("</body></html>");
        }
    }
}