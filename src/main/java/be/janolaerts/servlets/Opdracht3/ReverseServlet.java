package be.janolaerts.servlets.Opdracht3;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/Reverse")
public class ReverseServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        request.setCharacterEncoding("UTF-8");
        String text = request.getParameter("text");
        StringBuilder sb = new StringBuilder(text);

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        try(PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html");
            out.println("<html><head><title>");
            out.println("Echo Servlet");
            out.println("</title></head><body>");
            out.println(sb.reverse());
            out.println("</body></html>");
        }
    }
}