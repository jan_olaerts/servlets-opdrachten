package be.janolaerts.servlets.Opdracht10;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/SelectCustomer")
public class SelectCustomerServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String page = "SelectConsumer.html";
        String consumption = request.getParameter("consumption");
        if(consumption != null) {
            int cons;
            try {
                cons = Integer.parseInt(consumption);
                if(cons < 2000) {
                    page = "SmallConsumer.html";
                } else {
                    page = "BigConsumer.html";
                }
            } catch(NumberFormatException nfe) {
                page = "Invalid.html";
            }
        }

        RequestDispatcher disp = request.getRequestDispatcher("/HTML/" + page);
        disp.forward(request, response);
    }
}