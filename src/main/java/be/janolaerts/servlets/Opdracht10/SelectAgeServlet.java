package be.janolaerts.servlets.Opdracht10;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/SelectAge")
public class SelectAgeServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String page = "SelectAge.html";
        String stringAge = request.getParameter("age");

        if(stringAge != null) {
            int age;
            try {
                age = Integer.parseInt(stringAge);
                if(age >= 0 && age < 10) page = "Child.html";
                else if(age > 10 && age < 20) page = "Teenager.html";
                else if(age >= 20) page = "Adult.html";
                else page = "InvalidAge.html";
            } catch(NumberFormatException nfe) {
                page = "InvalidAge.html";
            }
        }

        RequestDispatcher disp = request.getRequestDispatcher("/HTML/" + page);
        disp.forward(request, response);
    }
}