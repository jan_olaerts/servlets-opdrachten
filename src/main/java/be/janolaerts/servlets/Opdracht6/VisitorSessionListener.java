package be.janolaerts.servlets.Opdracht6;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class VisitorSessionListener implements HttpSessionListener, ServletContextListener {

    public static final String TOTAL = "visitorsTotal";
    public static final String ACTIVE = "visitorsActive";

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute(TOTAL, 0);
        sce.getServletContext().setAttribute(ACTIVE, 0);
    }

    public void sessionCreated(HttpSessionEvent se) {
        ServletContext sc = se.getSession().getServletContext();

        Integer total = (Integer) sc.getAttribute(TOTAL);
        sc.setAttribute(TOTAL, ++total);

        Integer active = (Integer) sc.getAttribute(ACTIVE);
        sc.setAttribute(ACTIVE, ++active);
    }

    public void sessionDestroyed(HttpSessionEvent se) {
        ServletContext sc = se.getSession().getServletContext();
        Integer active = (Integer) sc.getAttribute(ACTIVE);

        sc.setAttribute(ACTIVE, --active);
    }
}