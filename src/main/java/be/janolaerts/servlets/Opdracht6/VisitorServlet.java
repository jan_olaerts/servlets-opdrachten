package be.janolaerts.servlets.Opdracht6;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/Visitors")
public class VisitorServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        request.getSession();
        int total = (Integer) getServletContext().getAttribute(VisitorSessionListener.TOTAL);
        int active = (Integer) getServletContext().getAttribute(VisitorSessionListener.ACTIVE);

        try(PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html><head><title>");
            out.println("Visitors");
            out.println("</title></head>");
            out.println("<body>");
            out.println("Total visitors: " + total + "<br/>");
            out.println("Active visitors: " + active + "<br/>");
            out.println("</body></html>");
        }
    }
}