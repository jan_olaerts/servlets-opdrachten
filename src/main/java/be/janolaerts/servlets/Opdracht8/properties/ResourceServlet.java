package be.janolaerts.servlets.Opdracht8.properties;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;

@WebServlet("/ResourceProps")
public class ResourceServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        try (InputStream in = getServletContext().getResourceAsStream("/WEB-INF/web.properties")) {
            Properties props = new Properties();
            props.load(in);

            try(PrintWriter out = response.getWriter()) {
                out.println("<!DOCTYPE html>");
                out.println("<html><head><title>");
                out.println("Resource Servlet");
                out.println("</title></head>");
                out.println("<body><h1>");
                out.println(props.getProperty("header"));
                out.println(props.getProperty("subtitle"));
                out.println(props.getProperty("text"));
                out.println("</h1></body></html>");
            }
        }
    }
}