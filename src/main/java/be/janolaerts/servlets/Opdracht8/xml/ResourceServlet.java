package be.janolaerts.servlets.Opdracht8.xml;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;

@WebServlet("/Resourcexml")
public class ResourceServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        try (InputStream in = getServletContext().getResourceAsStream("/WEB-INF/properties.xml")) {
            Properties props = new Properties();
            props.loadFromXML(in);

            try(PrintWriter out = response.getWriter()) {
                out.println("<!DOCTYPE html>");
                out.println("<html><head><title>");
                out.println("Resource Servlet");
                out.println("</title></head>");
                out.println("<body><h1>");
                out.println("<h1>" + props.getProperty("header") + "</h1>");
                out.println("<h2>" + props.getProperty("subtitle") + "</h2>");
                out.println("<p>" + props.getProperty("text") + "</p>");
                out.println("</h1></body></html>");
            }
        }
    }
}