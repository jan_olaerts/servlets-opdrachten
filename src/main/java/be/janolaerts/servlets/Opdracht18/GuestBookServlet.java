package be.janolaerts.servlets.Opdracht18;

import javax.servlet.ServletException;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

@WebServlet(name = "be.janolaerts.servlets.Opdracht18.GuestBookServlet",
            value = "/Guests18",
            initParams = {
                @WebInitParam(name="driver", value="org.mariadb.jdbc.Driver"),
                @WebInitParam(name="url", value="jdbc:mariadb://javadev-training.be:3306/javadevt_Hever7"),
                @WebInitParam(name="user", value="javadevt_StudHe"),
                @WebInitParam(name="password", value="STUDENTvj2020")
            }
)
public class GuestBookServlet extends HttpServlet {

    GuestBookDao guestBookDao = new GuestBookDao();

    public void init() {

        guestBookDao.setDriver(getInitParameter("driver"));
        guestBookDao.setUrl(getInitParameter("url"));
        guestBookDao.setUser(getInitParameter("user"));
        guestBookDao.setPassword(getInitParameter("password"));
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        try {
            List<GuestBook> guestBookList = guestBookDao.getGuestBookItems();
            String user = request.getRemoteUser();

            try(PrintWriter out = response.getWriter()) {
                out.println("<!DOCTYPE html");
                out.println("<html><head><title>");
                out.println("GuestBook Servlet");
                out.println("</title></head><body>");
                out.println("<h1> Guest List: </h1>");
                guestBookList.forEach(g -> {
                    out.println("<p> " + g.getName() + " " + " " + g.getDate() + "</p>");
                    out.println("Message: " + g.getMessage());
                    out.println("<p>---------------------------------------------------</p>");
                });

                if(request.isUserInRole("Guests")) {
                    // add form
                    out.println("<form method='POST'>");
                    out.println("Add a message: ");
                    out.println("<textarea type='text' name='name' placeholder='name' ></textarea>" );
                    out.println("<textarea type='text' name='message' placeholder='Write message' ></textarea>" );
                    out.println("<input type='submit' value='send' />");
                    out.println("</form>");
                    out.println("<a href='Logout' >Logout</a>");
                } else {
                    out.println("<p>You are not a guest</p>");
                    out.println("<p>You are not allowed to add messages</p>");

                    if(user != null) out.println("<a href='Logout' >Logout</a>");
                    else out.println("<a href='Logon' >Logon</a>");
                }

                out.println("</body></html>");
            }
        } catch (SQLException se) {
            throw new ServletException(se);
        }

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        try {
            String name = request.getParameter("name");
            String message = request.getParameter("message");
            Date now = Date.valueOf(LocalDate.now());
            guestBookDao.addGuestBookItem(new GuestBook(name, now, message));
        } catch (SQLException se) {
            throw new ServletException(se);
        }

        response.sendRedirect(request.getRequestURI());
    }
}