package be.janolaerts.servlets.Opdracht13;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/CookieName")
public class NameCookieServlet extends HttpServlet {

    private static final String FIRST_NAME = "firstname";
    private static final String LAST_NAME = "lastname";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String firstName = "";
        String lastName = "";

        // Get first and last name from cookie
        Cookie[] cookies = req.getCookies();

        if (cookies != null) {

            for (Cookie cookie : cookies) {
                if(cookie.getName().equals(FIRST_NAME)) {
                    firstName = cookie.getValue();
                    continue;
                }

                if(cookie.getName().equals(LAST_NAME)) {
                    lastName = cookie.getValue();
                }
            }
        }

        try (PrintWriter out = resp.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html><head><title>");
            out.println("Name Cookie Servlet");
            out.println("</title></head>");
            out.println("<body><h1>First and last name</h1>");
            out.println("<form method='POST'>");
            out.println("<input type='text' name='" + FIRST_NAME + "' placeholder='First name' value='" + firstName + "'>");
            out.println("<input type='text' name='" + LAST_NAME + "' placeholder='Last name' value='" + lastName + "'>");
            out.println("<input type='submit' value='send'>");
            out.println("</form>");
            out.println("</body></html>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        // Get first and last name from request parameter
        String firstName = req.getParameter(FIRST_NAME);
        String lastName = req.getParameter(LAST_NAME);

        if (!firstName.isBlank()) {
            // Create new cookie
            Cookie cookie = new Cookie(FIRST_NAME, firstName);
            cookie.setMaxAge(60);
            resp.addCookie(cookie);
        }

        if (!lastName.isBlank()) {
            // Create new cookie
            Cookie cookie = new Cookie(LAST_NAME, lastName);
            cookie.setMaxAge(60);
            resp.addCookie(cookie);
        }

        resp.sendRedirect(req.getRequestURI());
    }
}