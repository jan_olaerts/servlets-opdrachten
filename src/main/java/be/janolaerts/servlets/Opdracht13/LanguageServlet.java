package be.janolaerts.servlets.Opdracht13;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.Optional;
import java.util.stream.Stream;

@WebServlet("/SelectLanguage")
public class LanguageServlet extends HttpServlet {

    public static final String LANGUAGE = "language";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String language = null;

        // Get language from cookie
        Cookie[] cookies = req.getCookies();
        if(cookies != null) {
            Optional<String> ol =
                    Stream.of(cookies)
                    .filter(c -> c.getName().equals(LANGUAGE))
                    .map(Cookie::getValue)
                    .findAny();

            if(ol.isPresent()) {
                language = URLDecoder.decode(ol.get(), "UTF-8");
            }
        }

        // Get language from request parameter
        if(language == null) {
            language = req.getParameter(LANGUAGE);
            if(language != null) {
                // Create new cookie
                Cookie cookie = new Cookie(LANGUAGE, language);
                cookie.setMaxAge(60);
                resp.addCookie(cookie);
            }
        }

        try(PrintWriter out = resp.getWriter()) {

            out.println("<html><header>");
            out.println("<title>Language Selection</title>");
            out.println("</header><body>");

            if(language == null) {

                // Print choices
                out.print("<a href='SelectLanguage?language=en'>English</a><br/>");
                out.print("<a href='SelectLanguage?language=nl'>Nederlands</a><br/>");
                out.print("<a href='SelectLanguage?language=fr'>Français</a><br/>");
            } else {

                switch(language) {
                    case "en": out.println("English version"); break;
                    case "nl": out.println("Nederlandse versie"); break;
                    case "fr": out.println("Version française"); break;
                }
            }

            out.println("</body></html>");
        }
    }
}