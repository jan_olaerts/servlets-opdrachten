package be.janolaerts.servlets.Opdracht11;

import java.sql.Date;

public class GuestBook {

    private String name;
    private Date date;
    private String message;

    public GuestBook(String name, Date date, String message) {
        this.name = name;
        this.date = date;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }
}