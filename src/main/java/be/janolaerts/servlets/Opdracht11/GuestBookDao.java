package be.janolaerts.servlets.Opdracht11;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GuestBookDao implements GuestBookDao_Interface {

    String driver;
    String url;
    String user;
    String password;

    private static final String SQL_GET_ALL_GUESTBOOKS = "SELECT * FROM Guestbooks;";
    private static final String SQL_ADD_GUESTBOOK =
            "INSERT INTO Guestbooks (name, date, message) VALUES (?, ?, ?);";

    public GuestBookDao(String driver, String url, String user, String password) {
        this.driver = driver;
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName(driver);
        return DriverManager.getConnection(url, user, password);
    }

    @Override
    public List<GuestBook> getGuestBookItems() {

        List<GuestBook> guestBooks = new ArrayList<>();

        try(Connection con = getConnection();
            Statement stmt = con.createStatement()) {
            try {
                ResultSet rs = stmt.executeQuery(SQL_GET_ALL_GUESTBOOKS);
                while (rs.next()) {
                    String name = rs.getString("name");
                    Date date = rs.getDate("date");
                    String message = rs.getString("message");

                    guestBooks.add(new GuestBook(name, date, message));
                }
            } catch(SQLException se) {
                System.out.println(se.getMessage());
            }

        } catch (SQLException | ClassNotFoundException se) {
            System.out.println(se.getMessage());
        }

        return guestBooks;
    }

    @Override
    public void addGuestBookItem(GuestBook guestBook) {

        Timestamp timeStamp;
        try (Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement(SQL_ADD_GUESTBOOK)) {

            try {
                timeStamp = new Timestamp(guestBook.getDate().getTime());
                stmt.setString(1, guestBook.getName());
                stmt.setTimestamp(2, timeStamp);
                stmt.setString(3, guestBook.getMessage());

                stmt.executeUpdate();
            } catch (SQLException se) {
                System.out.println(se.getMessage());
            }

        } catch (SQLException | ClassNotFoundException se) {
            System.out.println(se.getMessage());
        }
    }
}