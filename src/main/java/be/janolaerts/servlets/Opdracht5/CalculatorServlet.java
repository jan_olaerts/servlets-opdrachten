package be.janolaerts.servlets.Opdracht5;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/Calculator")
public class CalculatorServlet extends HttpServlet {

    private final String RESULT = "CalculatorServlet.result";
    private final String NUMBER = "number";
    private final String ADD = "add";
    private final String SUBTRACT = "subtract";
    private final String MULTIPLY = "multiply";
    private final String DIVIDE = "divide";

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException {

        HttpSession session = request.getSession();
        Object resultAttribute = session.getAttribute(RESULT);

        int result = 0;
        if(resultAttribute != null) {
            result = (Integer) resultAttribute;
        }

        String message = "";
        Object messageAttribute = request.getAttribute("message");
        if(messageAttribute != null) {
            message = (String) messageAttribute;
        }

        try(PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html");
            out.println("<html><head><title>Calculator");
            out.println("</title></head></body>");
            out.print("<form method='POST' action='" + response.encodeURL(request.getRequestURI()) + "'>");
            out.print(message + "<br/>");
            out.println("Result: " + result + "<br/>");

            out.println("<input type='number' name='number' /><br/>");
            out.println("<input type='submit' name='submit' value='+' />");
            out.println("<input type='submit' name='submit' value='-' />");
            out.println("<input type='submit' name='submit' value='*' />");
            out.println("<input type='submit' name='submit' value='/' />");
            out.println("<input type='submit' name='submit' value='CE' />");

            out.println("</form></body></html>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws IOException {

        int result = 0;
        String message = "";
        HttpSession session = request.getSession();

        Object resultAttribute = session.getAttribute(RESULT);
        if(resultAttribute != null) {
            result = (Integer) resultAttribute;
        }

        String operation = request.getParameter("submit");
        if(operation != null) {

            String numberParameter = request.getParameter(NUMBER);
            if(numberParameter != null) {
                try {
                    if(operation.equals("+")) result += Integer.parseInt(numberParameter);
                    if(operation.equals("-")) result -= Integer.parseInt(numberParameter);
                    if(operation.equals("*")) result *= Integer.parseInt(numberParameter);
                    if(operation.equals("/")) result /= Integer.parseInt(numberParameter);
                    if(operation.equals("CE")) result = 0;
                } catch(NumberFormatException nfe) {
                    message = "Invalid number";
                }
            }
        }

        request.setAttribute("message", message);
        session.setAttribute(RESULT, result);
        doGet(request, response);
    }
}