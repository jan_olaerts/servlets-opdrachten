package be.janolaerts.servlets.Opdracht17;

import java.util.List;

public interface GuestBookDao_Interface {

    List<GuestBook> getGuestBookItems();
    void addGuestBookItem(GuestBook guestBook);
}