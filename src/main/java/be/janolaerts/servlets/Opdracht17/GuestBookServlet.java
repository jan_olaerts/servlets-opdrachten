package be.janolaerts.servlets.Opdracht17;

import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@WebServlet(name = "be.janolaerts.servlets.Opdracht17.GuestBookServlet",
            value = "/Guests17",
            initParams = {
                @WebInitParam(name="driver", value="org.mariadb.jdbc.Driver"),
                @WebInitParam(name="url", value="jdbc:mariadb://javadev-training.be:3306/javadevt_Hever7"),
                @WebInitParam(name="user", value="javadevt_StudHe"),
                @WebInitParam(name="password", value="STUDENTvj2020")
            }
)
@ServletSecurity(httpMethodConstraints =
        @HttpMethodConstraint(value = "POST", rolesAllowed = "Guests"))
public class GuestBookServlet extends HttpServlet {

    GuestBookDao guestBookDao = new GuestBookDao();

    public void init() {
        guestBookDao.setDriver(getInitParameter("driver"));
        guestBookDao.setUrl(getInitParameter("url"));
        guestBookDao.setUser(getInitParameter("user"));
        guestBookDao.setPassword(getInitParameter("password"));
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        List<GuestBook> guestBookList = guestBookDao.getGuestBookItems();

        try(PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html");
            out.println("<html><head><title>");
            out.println("GuestBook Servlet");
            out.println("</title></head><body>");
            out.println("<h1> Guest List: </h1>");
            guestBookList.forEach(g -> {
                out.println("<p> " + g.getName() + " " + " " + g.getDate() + "</p>");
                out.println("Message: " + g.getMessage());
                out.println("<p>---------------------------------------------------</p>");
            });

            // add form
            out.println("<form method='POST'>");
            out.println("Add a message: ");
            out.println("<textarea type='text' name='name' placeholder='name' ></textarea>" );
            out.println("<textarea type='text' name='message' placeholder='Write message' ></textarea>" );
            out.println("<input type='submit' value='send' />");
            out.println("</form>");

            out.println("</body></html>");
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        if(request.isUserInRole("Guests")) {
            String name = request.getParameter("name");
            String message = request.getParameter("message");
            Date now = Date.valueOf(LocalDate.now());
            guestBookDao.addGuestBookItem(new GuestBook(name, now, message));
        }

        response.sendRedirect(request.getRequestURI());
    }
}