package be.janolaerts.servlets.Opdracht7;

import java.util.List;

public interface GuestBookDao_Interface {

    List<GuestBook> getGuestBookItems();
    void addGuestBookItem(GuestBook guestBook);
}
