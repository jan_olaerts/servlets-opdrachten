package be.janolaerts.servlets.Opdracht7;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class GuestbookSessionListener implements HttpSessionListener, ServletContextListener {

    public final static String GUEST_BOOK_DAO = "guestBookDao";
    GuestBookDao guestBookDao;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String driver = sce.getServletContext().getInitParameter("driver");
        String url = sce.getServletContext().getInitParameter("url");
        String user = sce.getServletContext().getInitParameter("user");
        String password = sce.getServletContext().getInitParameter("password");

        guestBookDao = new GuestBookDao(driver, url, user, password);
        sce.getServletContext().setAttribute(GUEST_BOOK_DAO, guestBookDao);
    }
}