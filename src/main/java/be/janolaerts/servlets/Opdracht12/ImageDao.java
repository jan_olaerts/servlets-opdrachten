package be.janolaerts.servlets.Opdracht12;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ImageDao implements ImageDaoInterface {

    String driver;
    String url;
    String user;
    String password;

    private static final String SQL_GET_ALL_IMAGES = "SELECT * FROM Images;";
    private static final String SQL_GET_IMAGE_BY_ID = "SELECT * FROM Images WHERE id=?;";
    private static final String SQL_INSERT_IMAGE = "INSERT INTO Images (name, image) VALUES (?,?);";

    public ImageDao(String driver, String url, String user, String password) {
        this.driver = driver;
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(driver);
        return DriverManager.getConnection(url, user, password);
    }

    @Override
    public List<Image> getAllImages() {
        List<Image> imageList = new ArrayList<>();

        try(Connection con = getConnection();
            Statement stmt = con.createStatement()) {

            ResultSet rs = stmt.executeQuery(SQL_GET_ALL_IMAGES);
            while (rs.next()) {
                try {
                    imageList.add(readResultSet(rs));
                } catch(SQLException se) {
                    System.out.println(se.getMessage());
                }

            }

        } catch (SQLException | ClassNotFoundException se) {
            System.out.println(se.getMessage());
        }

        return imageList;
    }

    @Override
    public Image getImageById(int id) throws SQLException, ClassNotFoundException {

        try(Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement(SQL_GET_IMAGE_BY_ID)) {

            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                try {
                    return readResultSet(rs);
                } catch (SQLException se) {
                    System.out.println(se.getMessage());
                }
            }

            return null;
        }
    }

    @Override
    public void saveImage(Image image) {

        try(Connection con = getConnection()) {

            try (PreparedStatement stmt = con.prepareStatement(SQL_INSERT_IMAGE)) {

                stmt.setString(1, image.getName());
                stmt.setBytes(2, image.getImage());

                stmt.executeUpdate();
            } catch (SQLException se) {
                System.out.println(se.getMessage());
            }


        } catch (SQLException | ClassNotFoundException se) {
            System.out.println(se.getMessage());
        }
    }

    public Image readResultSet(ResultSet rs) throws SQLException {
        Integer id = rs.getInt("id");
        String name = rs.getString("name");
        byte[] image = rs.getBytes("image");
        return new Image(id, name, image);
    }
}