package be.janolaerts.servlets.Opdracht12;

public class Image {

    private Integer id;
    private String name;
    private byte[] image;

    public Image(Integer id, String name, byte[] image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getImage() {
        return image;
    }

    public void setBytes(byte[] image) {
        this.image = image;
    }
}