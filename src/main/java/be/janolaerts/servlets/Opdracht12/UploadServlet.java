package be.janolaerts.servlets.Opdracht12;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.OutputStream;

@WebServlet("/Upload")
@MultipartConfig(location="C:\\Development\\Software Development\\Software Development Courses and Tutorials\\" +
        "Java Courses and Tutorials\\Java Enterprise Ontwikkelaar VDAB CEVORA\\Servlets Opdrachten\\" +
        "src\\main\\webapp\\Images")
public class UploadServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher disp = request.getRequestDispatcher("/HTML/JPEGUpload.html");
        disp.forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("image/jpeg");
        Part part = request.getPart("uploadfile");
        byte[] bytes = part.getInputStream().readAllBytes();

        try(OutputStream out = response.getOutputStream()) {
            out.write(bytes);
        }
    }
}