package be.janolaerts.servlets.Opdracht12;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;
import java.util.List;

@WebServlet(value = "/UploadDB",
        initParams = {
                @WebInitParam(name="driver", value="org.mariadb.jdbc.Driver"),
                @WebInitParam(name="url", value="jdbc:mariadb://javadev-training.be:3306/javadevt_Hever7"),
                @WebInitParam(name="user", value="javadevt_StudHe"),
                @WebInitParam(name="password", value="STUDENTvj2020")
        }
)
@MultipartConfig
public class UploadDBServlet extends HttpServlet {

    ImageDao imageDao;
    private Boolean fileUploaded = null;

    public void init() {
        String driver = getInitParameter("driver");
        String url = getInitParameter("url");
        String user = getInitParameter("user");
        String password = getInitParameter("password");
        imageDao = new ImageDao(driver, url, user, password);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int count = 0;

        response.setContentType("text/html");
        try (PrintWriter out = response.getWriter()) {
            out.println("<DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>ImageDB</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>ImageList</h1>");
            List<Image> images = imageDao.getAllImages();
            if (images.size() > 0) {
                for (Image item : images) {
                    String imgBytes = new String(Base64.getEncoder().encode(item.getImage())); // Encode for setting in the <img> Tag
                    out.println("<img src=\"data:image/jpeg;base64," + imgBytes + "\" style=\"width: 200px; height: 150px;\">");
                    if(++count % 3 == 0) out.println("<br/>");
                }
            } else {
                out.print("<p>No Images in database</p>");
            }
            out.println("<form method=\"POST\" enctype=\"multipart/form-data\">");
            out.println("<label for=\"cons\">Select file:</label><br>");
            out.println("<input type=\"file\" id=\"cons\" name=\"uploadfile\" accept=\"image/jpeg\"><br><br>");
            out.println("<input type=\"submit\" value=\"Submit\">");
            out.println("</form>");
            if(!fileUploaded) out.println("<p>File was not uploaded</p>");
            else out.println("<p>File was uploaded!</p>");
            out.println("</body>");
            out.println("</html>");
        }

        fileUploaded = null;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Part part = request.getPart("uploadfile");
        ServletContext sc = getServletContext();
        String mimeType = sc.getMimeType(part.getSubmittedFileName());

        if(!mimeType.matches("(.*)(jpeg|jpg|png)")) {
            fileUploaded = false;
        } else {
            Image image = new Image(null, part.getSubmittedFileName(), part.getInputStream().readAllBytes());
            imageDao.saveImage(image);
            fileUploaded = true;
        }

        response.sendRedirect(request.getRequestURI());
    }
}