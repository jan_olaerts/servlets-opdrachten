package be.janolaerts.servlets.Opdracht12;

import java.sql.SQLException;
import java.util.List;

public interface ImageDaoInterface {
    List<Image> getAllImages();
    Image getImageById(int id) throws SQLException, ClassNotFoundException;
    void saveImage(Image image);
}